#include <incmode>. % built-in incremental mode of clingo

#show. % output atoms will be declared below

#program base. % static rules go below

holds(S,0) :- init(S).

assign(E,G,0) :- init(request(deliver(E),G)).
1 = { assign(E,G,0) : agent(elevator(E)) } :- init(request(call(D),G)), not init(request(deliver(_),G)).

assign(E,G) :- assign(E,G,0).
assign(E,F) :- init(at(elevator(E),F)).

max(E,F) :- agent(elevator(E)), F = #max { G : assign(E,G) }.
min(E,F) :- agent(elevator(E)), F = #min { G : assign(E,G) }.

inter(E,Gmax) :- max(E,Gmax), min(E,Gmin), init(at(elevator(E),F)), Gmax > F, Gmin < F, |F-Gmax| <= |F-Gmin|.
inter(E,Gmin) :- max(E,Gmax), min(E,Gmin), init(at(elevator(E),F)), Gmax > F, Gmin < F, |F-Gmax| > |F-Gmin|.
inter(E,Gmax) :- max(E,Gmax), min(E,Gmin), init(at(elevator(E),F)), Gmax >= F, Gmin >= F.
inter(E,Gmin) :- max(E,Gmax), min(E,Gmin), init(at(elevator(E),F)), Gmax <= F, Gmin <= F.
final(E,Gmin) :- max(E,Gmax), min(E,Gmin), init(at(elevator(E),F)), Gmax > F, Gmin < F, |F-Gmax| <= |F-Gmin|.
final(E,Gmax) :- max(E,Gmax), min(E,Gmin), init(at(elevator(E),F)), Gmax > F, Gmin < F, |F-Gmax| > |F-Gmin|.
final(E,Gmax) :- max(E,Gmax), min(E,Gmin), init(at(elevator(E),F)), Gmax >= F, Gmin >= F.
final(E,Gmin) :- max(E,Gmax), min(E,Gmin), init(at(elevator(E),F)), Gmax <= F, Gmin <= F.

moves(E,|F-G0|+|G0-G1|) :- init(at(elevator(E),F)), inter(E,G0), final(E,G1).

goal(E,G,0) :- inter(E,G).

#program step(t). % actions, effects, and minimization go below

serve(E,F,t) :- holds(at(elevator(E),F),t-1), assign(E,F,t-1).

do(elevator(E),serve,t) :- serve(E,_,t).
do(elevator(E),move((G-F)/|G-F|),t) :- holds(at(elevator(E),F),t-1), not assign(E,F,t-1), goal(E,G,t-1), G != F.

assign(E,G,t) :- assign(E,G,t-1), holds(at(elevator(E),F),t-1), F != G.

goal(E,G,t) :- holds(at(elevator(E),F),t-1), goal(E,G,t-1), G != F.
goal(E,H,t) :- holds(at(elevator(E),G),t-1), goal(E,G,t-1), final(E,H).

holds(at(elevator(E),F),t) :- holds(at(elevator(E),F),t-1), not do(elevator(E),move(_),t).
holds(at(elevator(E),F+M),t) :- holds(at(elevator(E),F),t-1), do(elevator(E),move(M),t).

holds(request(deliver(E),F),t) :- holds(request(deliver(E),F),t-1), not serve(E,F,t).
holds(request(call(D),F),t)    :- holds(request(call(D),F),t-1), not serve(_,F,t).

#minimize { M,E : moves(E,M)}.

#program check(t). % fulfillment of all requests can, e.g., be checked as follows

:- query(t), holds(request(_,_),t).

#show holds/2.
#show do/3.
