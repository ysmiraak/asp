#!/usr/bin/env python3


import re


def parse(lines, re_atom= re.compile(r"at\(.*?\)")):
    ans = []
    for line in lines:
        match = re_atom.findall(line)
        if match: ans.append(frozenset(match))
    return frozenset(ans)


if '__main__' == __name__:
    import subprocess
    import sys

    arg = dict(zip(sys.argv[1::2], sys.argv[2::2]))
    arg.setdefault('--lvl', '12')
    arg.setdefault('--mino', "mino.lp")
    arg.setdefault('--timeout', "0")
    print(arg)

    timeout = int(arg['--timeout']) or None
    time = 0.0
    for lvl in range(1, 1 + int(arg['--lvl'])):
        path = 'level{:02d}.lp'.format(lvl)
        with open(path, encoding= 'utf-8') as file: gold = parse(file)
        try:
            out, err = subprocess.Popen(
                ('clingo', '--quiet=1', '--opt-mode=optN', arg['--mino'], path)
                , stdout= subprocess.PIPE
                , universal_newlines= True).communicate(timeout= timeout)
        except subprocess.TimeoutExpired:
            print("timeout", path)
        test = parse(out.splitlines())
        if test != gold:
            print("failed", path)
            print("expected:")
            for m in gold: print(*m)
            print("got:")
            for m in test: print(*m)
            sys.exit()
        else:
            t = out[out.rindex("CPU Time") + 15:-2]
            time += float(t)
            print("passed", path, t)
    print(time)
