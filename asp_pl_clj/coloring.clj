(comment ;; brute force
  (let [colors [:r :b :g]]
    (for [c1 colors c2 colors c3 colors c4 colors c5 colors c6 colors
          :when (and (not= c1 c2)
                     (not= c1 c3)
                     (not= c1 c4)
                     (not= c2 c4)
                     (not= c2 c5)
                     (not= c2 c6)
                     (not= c3 c1)
                     (not= c3 c4)
                     (not= c3 c5)
                     (not= c4 c1)
                     (not= c4 c2)
                     (not= c5 c3)
                     (not= c5 c4)
                     (not= c5 c6)
                     (not= c6 c2)
                     (not= c6 c3)
                     (not= c6 c5))]
      [c1 c2 c3 c4 c5 c6]))
  =>
  [:b :g :g :r :b :r]
  [:b :r :r :g :b :g]
  [:g :b :b :r :g :r]
  [:g :r :r :b :g :b]
  [:r :b :b :g :r :g]
  [:r :g :g :b :r :b])

(library :logic*)

(comment ;; hard code
  (q/run* [c1 c2 c3 c4 c5 c6]
    (fd/in c1 c2 c3 c4 c5 c6 (fd/interval 1 3))
    (q/!= c1 c2)
    (q/!= c1 c3)
    (q/!= c1 c4)
    (q/!= c2 c4)
    (q/!= c2 c5)
    (q/!= c2 c6)
    (q/!= c3 c1)
    (q/!= c3 c4)
    (q/!= c3 c5)
    (q/!= c4 c1)
    (q/!= c4 c2)
    (q/!= c5 c3)
    (q/!= c5 c4)
    (q/!= c5 c6)
    (q/!= c6 c2)
    (q/!= c6 c3)
    (q/!= c6 c5))
  =>
  [1 2 2 3 1 3]
  [1 3 3 2 1 2]
  [2 1 1 3 2 3]
  [2 3 3 1 2 1]
  [3 1 1 2 3 2]
  [3 2 2 1 3 1])

(def graph
  #{[1 2] [1 3] [1 4]
    [2 4] [2 5] [2 6]
    [3 1] [3 4] [3 5]
    [4 1] [4 2]
    [5 3] [5 4] [5 6]
    [6 2] [6 3] [6 5]})

(q/run* [c1 c2 c3 c4 c5 c6]
  (fd/in c1 c2 c3 c4 c5 c6 (fd/interval 1 3))
  (let [color [nil c1 c2 c3 c4 c5 c6]]
    (q/and* (map (fn [[n m]] (q/!= (color n) (color m))) graph))))
