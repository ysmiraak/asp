(library :logic*)

(pldb/db-rel motive p)
(pldb/db-rel guilty p)

(def db-sus
  (pldb/db
   [motive :harry]
   [motive :sally]
   [guilty :harry]))

(defn innocento [q]
  (q/fresh [p]
    (motive q)
    (guilty p)
    (q/!= q p)))

(pldb/with-db db-sus
  (q/run* [q]
    (innocento q)))
