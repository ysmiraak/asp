innocent(Suspect) :- motive(Suspect), \+ guilty(Suspect).

motive(harry).
motive(sally).
guilty(harry).

%% ?- innocent(X).
%@ X = sally.
