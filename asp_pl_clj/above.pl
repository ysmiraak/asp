on(a,b).
on(b,c).

above(X,Y) :- on(X,Y).
above(X,Y) :- on(X,Z), above(Z,Y).

%% ?- above(a,c).

%% ?- above(c,a).

%% ?- above(X,Y).
