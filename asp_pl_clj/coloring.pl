edge(1,2).
edge(1,3).
edge(1,4).
edge(2,4).
edge(2,5).
edge(2,6).
edge(3,1).
edge(3,4).
edge(3,5).
edge(4,1).
edge(4,2).
edge(5,3).
edge(5,4).
edge(5,6).
edge(6,2).
edge(6,3).
edge(6,5).

color(r).
color(g).
color(b).

set_color([], []).

set_color([H | T], [H/C | TC]) :-
    set_color(T, TC),
    color(C),
    forall(member(Node/Color, TC),
           ((edge(Node, H) -> Color \= C; true),
            (edge(H, Node) -> Color \= C; true))).

coloring(L) :-
    setof(Node,X^Y^(edge(Node, X); edge(Y,Node)), LN),
    set_color(LN, L).

%% ?- coloring(X).
%@ X = [1/g, 2/b, 3/b, 4/r, 5/g, 6/r] ;
%@ X = [1/b, 2/g, 3/g, 4/r, 5/b, 6/r] ;
%@ X = [1/r, 2/b, 3/b, 4/g, 5/r, 6/g] ;
%@ X = [1/b, 2/r, 3/r, 4/g, 5/b, 6/g] ;
%@ X = [1/r, 2/g, 3/g, 4/b, 5/r, 6/b] ;
%@ X = [1/g, 2/r, 3/r, 4/b, 5/g, 6/b] ;
%@ false.


% b g g r b r
% b r r g b g
% g b b r g r
% g r r b g b
% r b b g r g
% r g g b r b
