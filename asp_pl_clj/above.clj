(library :logic*)

(pldb/db-rel on x y)
(def db-on
  (pldb/db
   [on :a :b]
   [on :b :c]))

(defn aboveo [x y]
  (q/conde
   [(on x y)]
   [(q/fresh [z]
      (on x z)
      (aboveo z y))]))

(pldb/with-db db-on
  (q/run* [q p]
    (aboveo q p)))
